from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from main.models import Twit, Article


class RegisterUserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    password2 = forms.CharField(widget=forms.PasswordInput)
    blogger = forms.BooleanField(initial=False, help_text="Хочете бути блогером?", required=False, label='Блогер')

    class Meta:
        model = User
        fields = ['username', 'email']

    # Validation password
    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise ValidationError("Password don't mach")

        return cd['password']


class FormTwitModel(forms.ModelForm):
    class Meta:
        model = Twit
        # exclude = ()
        fields = ['text', ]


class ArticleCreateForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ['title', 'text', ]