from django.db import models
from django.contrib.auth.models import User


class Blogger(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    blogger = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = "Profile"
        verbose_name_plural = "Profiles"


class Article(models.Model):
    author = models.ForeignKey(Blogger, on_delete=models.CASCADE, blank=False, verbose_name="Автор")
    title = models.CharField(max_length=120, help_text="Основна ідея поста", verbose_name="Заголовок")
    text = models.TextField(max_length=1000, verbose_name="Текст")
    pub_date = models.DateTimeField(auto_now=True, verbose_name="Дата публікації")

    class Meta:
        ordering = ['-pub_date']

    def __str__(self):
        return "Article:" + self.title + " " + str(self.pub_date)


class Twit(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, verbose_name="Загловок")
    autor = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="")
    text = models.CharField(max_length=150, help_text="Комент")
    pud_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Twtit on Article " + self.article.title + " by the user " + self.autor.username + " " + str(self.pud_date)

    @property
    def date(self):
        return self.pud_date

