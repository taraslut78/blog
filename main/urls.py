from django.urls import path
from . import views

app_name = "blog-main"

urlpatterns = [
    path('', views.PostList.as_view(), name="artile-list"),
    path('<int:pk>', views.PostDetails.as_view(), name='article-detail'),
    path('save_twit', views.save_twit, name='twit-save'),
    path('registrate_user', views.UserReg.as_view(), name="user-reg"),
    path('blogger', views.BloggerList.as_view(), name='blogger-list'),
    path('blogger/<int:pk>', views.BloggerArticlesList.as_view(), name='blogger-article-list'),
    path('create', views.CreateNewArticle.as_view(), name='new-article'),
]
