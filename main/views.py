from django.contrib import messages
from django.contrib.admin.templatetags.admin_list import pagination
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.shortcuts import redirect
from django.views import generic

from .forms import RegisterUserForm, FormTwitModel, ArticleCreateForm
from .models import Article, Twit, Blogger


class PostList(generic.ListView):
    model = Article
    paginate_by = 5
    # queryset = Article.objects.filter(text__contains=self.request.GET.get('q') )

    def get_queryset(self):
        result = super(PostList, self).get_queryset()
        if self.request.GET.get('q'):
            result = result.filter(text__contains=self.request.GET.get('q'))
        return result


class PostDetails(generic.DetailView):
    model = Article

    def get_context_data(self, **kwargs):
        context = super(PostDetails, self).get_context_data(**kwargs)
        context['form_twit'] = FormTwitModel
        return context


@login_required
def save_twit(request):
    if request.POST:
        print(request.POST)
        twit = Twit()
        twit.article_id = int(request.POST['pk'][0])
        twit.autor_id = int(request.POST['username'][0])
        twit.text = request.POST['text'].strip()
        if len(twit.text.strip())<2:
            messages.success(request, "Sorry. Your comment is wrong.")
            return redirect(to=request.POST['url'])
        if Twit.objects.filter(text=request.POST['text']):
            messages.success(request, "Sorry. There is such comment")
        else:
            twit.save()
        twit.url = request.POST['url']
        return redirect(to=twit.url)
    else:
        redirect(to='/')


class UserReg(generic.CreateView):
    form_class = RegisterUserForm
    template_name = 'main/user_registration.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseForbidden()

        return super(UserReg, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        user = form.save(commit=False)
        user.set_password(form.cleaned_data['password'])
        user.save()
        Blogger.objects.create(user=user, blogger=form.cleaned_data['blogger'])
        login(request=self.request, user=user)
        return redirect(to='/')


class BloggerList(generic.ListView):
    model = Blogger
    queryset = Blogger.objects.exclude(user__is_active=False).exclude(blogger=False)


class BloggerArticlesList(generic.DetailView):
    model = Blogger
    paginate_by = 5


class CreateNewArticle(generic.CreateView, LoginRequiredMixin):
    model = Article
    form_class = ArticleCreateForm

    def form_valid(self, form):
        article = form.save(commit=False)
        article.author_id = int(self.request.POST['author'])
        if len(Article.objects.filter(title=article.title)) and len(article.text.strip())>10:
            messages.success(self.request, "Sorry there is such Article")
        else:
            article.save()
        return redirect(to='/')

