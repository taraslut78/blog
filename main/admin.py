from django.contrib import admin
from . import models


class ArticleAdmin(admin.ModelAdmin):
    exclude = ()


class TwitAdmin(admin.ModelAdmin):
    exclude = ()


class AdminAdditionUserModel(admin.ModelAdmin):
    exclude = ()


admin.site.register(models.Article, ArticleAdmin)
admin.site.register(models.Twit, TwitAdmin)
admin.site.register(models.Blogger, AdminAdditionUserModel)
